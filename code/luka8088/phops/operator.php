<?php

namespace luka8088\phops;

function toString ($value) {
  assert(count(func_get_args()) == 1);
  if (is_object($value) && hasMember($value, 'toString'))
    return $value->toString();
  if (is_array($value))
    return '[' . implode(', ', array_map(function ($value) { return toString($value); }, $value)) . ']';
  return (string) $value;
}
