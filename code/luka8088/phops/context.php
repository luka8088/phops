<?php

namespace luka8088\phops;

function context ($type, $value = null, $callback = null) {

  static $stacks = [];

  if (!$value && !$callback) {
    assert(isset($stacks[$type]) && count($stacks[$type]) > 0, 'Context *' . $type . '* not set.');
    return end($stacks[$type]);
  }

  if (!hasMember($stacks, $type)) {
    $stacks[$type] = [];
  }

  array_push($stacks[$type], $value);
  try {
    return call_user_func($callback);
  } finally {
    array_pop($stacks[$type]);
    if (count($stacks[$type]) == 0)
      unset($stacks[$type]);
  }

}
