<?php

namespace luka8088\phops;

use \ErrorException;
use \Exception;
use \Throwable;

function assertThrown ($throwable = 'Exception', $callback = null, $message = 'Assertion failure.') {
  try {
    $callback();
  } catch (Throwable $e) {
    if (is_a($e, $throwable, true))
      return;
  } catch (Exception $e) {
    if (is_a($e, $throwable, true))
      return;
  }
  assert(false, $message);
}

// Enforcement indicates a data error (caused by, for example, external input).
// Enforcements will never be compiled out.
// enforce ([$class,] $condition, $message);
function enforce ($condition, $message) {

  $class = 'Exception';
  $args = func_get_args();

  if (!is_bool($condition)) {
    assert(count($args) == 3, 'Wrong enforce arguments type or count.');
    $class = $args[0];
    $condition = $args[1];
    $message = $args[2];
  }

  assert(is_string($class) || is_object($class), 'Class or object instance required.');
  assert(is_bool($condition), 'Condition must be bool.');
  assert(is_string($message), 'Message must be a string.');
  assert(count(func_get_args()) <= 3, 'enforce accepts exactly max three arguments.');

  if (!$condition)
    throw new $class($message);
}

function initializeStrictness () {
  set_exception_handler('\luka8088\phops\_phops_uncaught_exception_handler');
  set_error_handler('\luka8088\phops\_phops_exception_error_handler');
  register_shutdown_function('\luka8088\phops\_phops_shutdown_error_handler');
}

function _phops_exception_error_handler ($errno, $errstr, $errfile, $errline) {
  new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

function _phops_shutdown_error_handler () {
  $error = error_get_last();
  if (isset($error)) {

    // Due to error_clear_last() being available only since PHP7 we assume that
    // empty error message is a clear state.
    if ($error['message'] == '')
      return;

    // Due to error_clear_last() being available only since PHP7 we assume that
    // "Undefined variable: undefinedVariable" is a clear state.
    if ($error['message'] == 'Undefined variable: undefinedVariable')
      return;

    // $error['type'] != E_WARNING is a workaround for error_get_last() being set even
    // if error has been handled by _phops_exception_error_handler - php bug ?
    if ($error['type'] == E_WARNING)
      return;

    // checking 'Uncaught exception ' is a workaround for error_get_last() being set even
    // if exception has been handled by set_exception_handler - php bug ?
    if (substr($error['message'], 0, strlen('Uncaught exception ')) == 'Uncaught exception ')
      return;

    _phops_uncaught_exception_handler(
      new ErrorException('PHP shutdown error: ' . $error['message'], 0, $error['type'], $error['file'], $error['line'])
    );

  }
}

function _phops_uncaught_exception_handler ($exception) {
  if (php_sapi_name() == 'cli')
    echo $exception;
  else
    echo '<pre>' . $exception . '</pre>';
  exit;
}
